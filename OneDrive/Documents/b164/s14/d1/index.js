console.log(
	"hello world"
	);

//console.log use to print out going to console
//variables
//it is use to contain data
//declaring variables
//syntax: let/const variablename;var
//let/const/var is a keyword that is usually used in declaring a variable
//let myvariable;



const myvariable = "hello again"
console.log(myvariable);
/*
guidelines in writing variables
1. we can use "let" keyword followed by the variable name of your choosing and use
the assignment operator (=) to assign value.
2. variable names should start with a lowercase character, use canelCase for for multiple words


Best practices in naming variables
1 when naming variabless , it is important to create variables that are descriptive and indicative of the data it contains
2. It is better to start with lower case letter because there are several keywords in JS that start in capital letter.
3. Do not add spaces to your variable names. use camelCase for multiple words, or underscore



*/

let firstName = "jane"
let lastName = "doe"
let pokemon = 25000

let product_description = "sample";

//declaring and initializing variables
//initializing variables - the instance when a variables is given its initial/starting value
//syntax: let/const variableName = value;

let productName = "desktop computer"
console.log (productName)

let productPrice = 18999;
console.log (productPrice)

const interest = 3.539

//reaassigning variables values

productName = "laptop"
console.log (productName);

let friend = 'kate'
let unfriend = 'john'
console.log(unfriend)
console.log(friend)

const pi= 3.14

console.log(pi)

//const cannot be updated or redeclared
// values of constants cannot be changed and will simply return an error

/// re assigning variables vs. initializing variables
// let - we can declare a variable first
let supplier;
// initialization is done after the variable has been declared
supplier = 'John Smith Tradings'
console.log(supplier)
 supplier= 'zuitt store'
 console.log (supplier)
 //const variables are variables with constant data. Therefore we should not re-declare, re-assign or even declare a const variable without initialization.
//var- is also declaring a variable. var is an ECMA script (ES1) feature ES1 (js 1997).
// let/const was introduced as new feature in ES6(2015).

//what makes let/const different from var? 

// there are issues when we use var in declaring variables, one of this is hoisting.
//Hoisting is JS default behavior of moving declaration to the top.


a = 5;
console.log(a)

var a

// Scope essentially means where these variables are available to use
// let/const can be use as a local and global scope

let outerVariable = "hello"
//block/local scope = {}

{
	
	let innerVariable = "hello agaiin"
	console.log (innerVariable)
}



//multiple variable declarations

let productCode = 'D0C17'
const productBrand = 'Dell'
console.log (productCode , productBrand)

//Data types
// Strings are a series of character that create a word, a phrase, a sentence or
//anything related to creating text.
// strings in javascript can be written using either a single (') or double ("")
let country ='philippines'
let province = "metro manila"
console.log(typeof country)
console.log(typeof country)

//Concatenating Strings 
// Mutiple strings values can be combined to create a single string using the"+""
//symbol
let fullAddress = province + ','+ country;
console.log (fullAddress);

let greeting = ' I Live in the ' + country;
console.log (greeting);

//template Literals (ES6) is the updated version of concatenation
//backticks `dvdv`
//expression ${}


console.log (`I Live the${province}, ${country}`)

//Escape character (\) in strings in combination with other characters can produce
//diffrent effects
//"\" refers to creating a new line in between text

let mailAddress = 'Metro Manila\n\nPhilippines';
console.log (mailAddress);

//\'

let message = 'john\'s employees went home early';
console.log(message);

message = "john's employees went home early;"
console.log(message)

console.log (
	`Metro Manila

	'hello'
	"hello"


	Philippines ${province}`
	
)
//Numbers
//Intergers/Whole Numbers
let headCount = 23;
console.log(typeof headCount);

let grade = 98.7;
console.log(typeof grade)

let planetDistance = 2e10;
console.log(typeof planetDistance);

//combining variable with number data type and string
console.log ("John's grade last quarter is" + grade);
console.log(`john's grade last quarter is ${grade}`);



//BOOLEAN
//Boolean

let isMarried = false;
let inGoodConduct = true;
console.log(typeof isMarried)
console.log(typeof inGoodconduct);


//OBJECTS

//Arrays 	
		// are a special kind of data that is used to store multiple values. it can store different data types but is normally used to store similar data types

		//similar data types
		// syntax: let/const arrayName = [elementA, elementB, etc]

		let grades = [98.7, 92.1, 90.2, 94.6]
		console.log(grades)
		console.log(typeof grades)


		//different data types
		//storing diff data types inside array is not recommended because it will not make sense to join the context of programming.
		let details = ["John", "Smith",32, true];
		console.log(details)

		console.log(grades[1] , grades[0])

		//Reassigning array elements

		let anime = ['one piece', 'one punch man' , 'attack on titan']
		console.log(anime)

		anime[0] = 'kimetsu no yaiba'
		console.log(anime);
		// the key const is little misleading . It does not define a constant value.It defines a constant reference to a value.

		//anime = {'kimetsu no yaiba','charlotte', 'solo leaving', 'boku no hero'}
		//console.log(anime)


		/*
			Beacause of this you can not:
			 reaasign constant value, constant array, constant object

			 But You Can;

			 change the elements of content array
			 change properties of constatn object



		*/
//objects 
//syntax
/*
	let/const objectName= {propertyA: value propertyB: value
	}
	They Are used to create complex data that contains pieces of information that are relevant to each other.
*/
let objectGrades = { 
	firstQuarter: 98.7,
	secondQuarter: 92.1,
	thirdQuarter: 90.2, 
	forthQuarter: 94.6
}

let person = {
	fullName: 'Juan Dela Cruz',
	age: '35',
	isMarried: false,
	contact: ['09167195063','8123 4567'],
	address: {
		houseNumber: '345',
		city: 'Manila'
	}

}
console.log(person)
console.log(person.address.city)


let numb1 = 500;
let numb2 = 6;
let numb3 =5.5;
let numb4 = .5;
let numString1 = "5";
let numString2 = "6";
 console.log(numString1 + numString2)
 console.log(numb1 + numb2)

 //type coercion/force coercion
 console.log (numString1 + numb1)

//Mathematical Operation (+,-,*,/ % (modulo-remainder))
console.log (numb1 % numb2)
console.log (numb2 % numb1)


console.log(numb2 - numb1)
let product = numb1 + numb2
console.log (product)

//Null
//it is use to intentionally the absence of a value in a variable



//Undefined
//represents the state of a variable that has been declared but without an assigned value.



let gF = null
console.log (gF)

let myNumber = 0
let myString = ''
console.log (myNumber)
console.log (myString)

let sampleVariable
console.log(sampleVariable)

//one clear difference between undefined and null is that for undefined, a variable was created but was not provided a value.
//null means that a variable  was created and was assigned value that does not hold any value/amount.

//function
// in Js, it is a line/s blocks of codes that tell our app to perform a certain task when called/invoked

// Function declaration
	//defines a function with the function names and specified parameters
	/*
	syntax:

	function functionName() {
	code block/function statements the block of codes will be executed one the function whas been run/called/invoked.
	}
	function keyword => defined a js function
	function name => camelCase to name our function
	function block/statements => the statements which comprise the body of the function. TYhis is where the code to be executed
	*/
 //function printName()
 
 //	console.log("My name is Mike");


 	// function expression
 		// a function expression can be stored in a variable
 		let variableFunction = function(){
 			console.log("hello world")
 		}
 //	 anonymous function - a function without a name.

 let funExpression = function Func_name() {
 	console. log ('hello!')
 }

//Function invocation
// the code inside a function is executed when the function is called/invoked.

// lets invoke/call the function that we declared
 printName() 

 function declaredFunction() {
 	console.log ("hello world")
 } 

 declaredFunction()
 
 // How about the function expression?
 // they are always invoked/called using the variables name

let name = "janes yap"

 //variableFunction()
 function showSum() {
 	console.log(6 + 6)
 }
 showSum()

 function printName(name)
 {
console.log(`My name is ${name}`);
}
printName(name)


function showqoutient(){
	console()
}
let firstNumber = 2
let number1 = 5
let number2 = 1

console.log ( `the result of the division is: ${firstNumber}`)
	