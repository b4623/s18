console.log ("Hello World")

let grades = [98.5, 91.2, 93.1 , 89.0]
console.log(grades[0])

let myTask = [
"drink html",
"eat java script",
"inhale css",
'bake express'
];

console.log('Array before reassigment')
console.log(myTask);

myTask[0] = "hello world"
console.log ("Array after reassignment")
console.log( myTask);
console.log(myTask[4])


let computerBrands=['Acer', "Asus", "Lenovo", "Neo", "Toshiba", "Gateway", "Redfox","fujitsu"]
console.log(computerBrands.length);
console.log(computerBrands)

if (computerBrands.length > 5){
	console.log("We have too many supplier. Please coordinate with the operations manager.")
}

let lastElementIndex = computerBrands.length -1;
console.log (computerBrands[lastElementIndex]);


let fruits = ["Apple", "Orange","Kiwi","Dragon Fruit"];

console.log ( 'Current Array:');
console.log (fruits);

let fruitsLength = fruits.push('Mango');
console.log("fruitslength");
console.log("mutated array from push method");
console.log(fruits);

fruits.push("Avocado","Guava")
console.log("mutated array from push method");
console.log(fruits)

let removedFruit = fruits.pop();
console.log(removedFruit);
console.log(fruits);
fruits.pop();
console.log(fruits);

fruits.unshift('Lime', "Banana")

console.log("Mutated array from unshift");
console.log(fruits);

let anotherFruit = fruits.shift();
console.log(anotherFruit)
console.log(fruits)

fruits.splice(1,2,"lime", "cherry");
console.log("splice method:");
console.log(fruits);

fruits.sort();
console.log('Sort method');
console.log(fruits)


fruits.reverse();
console.log("reverse method");
console.log(fruits)


let countries= ["Philippines", "Japan"," Canada", "Thailand", "France", "Denmark"]

let firstIndex= countries.indexOf("Philippines");
console.log(`result of indexOf method: ${firstIndex}`)

let invalidCountry = countries.indexOf("Japan");
console.log(`indexOf: ${invalidCountry}`)

let lastIndex = countries.lastIndexOf("Philippines");
console.log( `lastIndexOf method: ${lastIndex}`);

let lastIndexStart = countries.lastIndexOf("Philippines", 4);
console.log(`lastIndexOf: ${lastIndexStart}`)

let slicedArrayA= countries.slice(2);
console.log(countries)
console.log("slice method:");
console.log(slicedArrayA);

let slicedArrayB = countries.slice(2, 4)
console.log(slicedArrayB)

let slicedArrayC = countries.slice(-3);
console.log(slicedArrayC)

let stringArray = countries.toString();
console.log("toString method:");
console.log(stringArray);

let taskArrayA = ["drink html", "eat javascript"];
let taskArrayB = ["inhale css", "breath sass"];
let taskArrayC = ["get git", "be node"];

let tasks = taskArrayA.concat(taskArrayB);
console.log(`concat method ${tasks}`)
console.log(tasks)

let allTasks = taskArrayA.concat(taskArrayB, taskArrayC);
console.log(allTasks);

let combinedTasks = taskArrayA.concat('smell express', "throw react");
console.log(combinedTasks)

let users = ["john", "Jane", "Joe", "Robert"];

console.log(users.join());
console.log(users.join(" "));
console.log(users.join("-"))



allTasks.forEach(function(task){
	console.log(task)
});

for (i= 0; i< allTasks.fruitsLength; i++){
	console.log(allTasks[i])
}

filteredTasks = []

allTasks.forEach(function(task) {
	if(task.length > 10){
		filteredTasks.push(task)
	}
})

console.log("Result of filtered tasks")
console.log(filteredTasks)

let sampleArray = ["eat", "drink"];

let data = prompt ("add a data");

sampleArray.unshift(data);

console.log(sampleArray)

/*arrayName.forEach(function(numbers) {
	if( numbers % 2 === 0){
		console.log("i am even number" + numbers)
	}else{
		console.log("i am an odd number" + numbers)
	}
})*/

let numbers = [1,2,3,4,5]

let numberMap = numbers.map(function(number){
	return number * number
})

console.log("Map method:")
console.log(numbers);
console.log(numberMap);

let allValid = numbers.every(function(number){
	return( number < 0);
})

console.log("every method:")
console.log(allValid);

let someValid = numbers.some(function(number){
	return (number < 3)
})

console.log("some method:");
console.log(someValid);

if(someValid){
	console.log("some of number in the array is greater than 2")
}


if(allValid) {
	console.log("All of numbers in the array are greater than 0");
}

let filterValid = numbers.filter(function(number){
	return ( number< 3);
})

console.log("filter method:");
console.log(filterValid)

let nothingFound = numbers.filter(function(number){
	return (number == 0)
})

console.log(nothingFound)



numbers.forEach(function(number){
	if(number < 3){
		filteredNumbers.push(number);
	}
})

let filteredNumbers = 

console.log(filteredNumbers)

let products = ["mouse", 'keyboard', 'laptop',
"monitor"];

let filteredProducts = products.filter(function(product){
	return product.toLowerCase().includes("a")
})

console.log(filteredProducts);

let iteration = 0;

let reducedArray = numbers.reduce(function(x,y){
	console.warn("current iteration" + ++iteration);
	console.log("accumulator: "+ x);
	console.log("currentValue: "+ y);
	return x + y
})

console.log('result of reduce method:'+ reducedArray);


let list = ["hello", "Again", "World"];

let reducedJoin = list.reduce(function(x,y){
	return x + " "+ y;
})

console.log("reduce method: " + reducedJoin);


let chessBoard = [
["a1", " b1", " c1", "d1", "e1"]
]